# SVN to Git Transition

---
# SVN Workflow

Below is the basic SVN workflow that you may be familiar with. This is what
you probably do 90% of the time.


To start working you `Checkout` the repository. Then for each change you:

1. `Update` to get the latest
2. Modify Your Content
3. `Commit` your changes

To clean you use `svn cleanup`.

---
# Git Workflow

Below is a close analog for what you would do in git:


To start working you `Clone` the repository. Then for each change you:

1. `Checkout` your branch and `Pull` updates
2. Modify Your Content
3. Stage, `Commit`, and `Push` your content

To clean you use `git reset --hard` and `git clean -df`.

---
# Git Differences

* Git keeps a copy of the repository on your local system.
    * You can commit without being connected to the (remote) repository.
* Git uses merging instead of lock step development
* Git does not use version numbers, instead it uses hashes 
  `8f00b4651434772651bfba013e02b2c4be2d02e7`
    * Git hashes can often be shortened to 7 characters `8f00b46`
* Tags and branches are first class objects and not just a "copy" of the 
  repository at a given state
* Git uses a "stage" to store changes that will be committed.
* Git Submodules are somewhat analogous to SVN externs
* You can't checkout a sub-tree with Git like you can with SVN. 

---
# Git Stage

* `git add .` to add all differences from the last commit (version).
    * You can specify only changes in a particular folder `git add ./subfolder`.
    * You can specify only changes in c files `git add *.c`.
* To remove everything from the stage use `git reset`
* If you add a file to the stage and make more changes to that file, you must
  re-add that file. Git tracks content not files.
* Only staged files will be committed when you run the `commit` command

---
# Git Merge

You are merging all the time with git. 

* Git `pull` and `push` commands merge in the background.
* Most of the time the merge is a "fast forward". 
    * For example if you push new changes to a remote repository and there was 
      no change to that repository since you last pulled from it.
* Other times git will merge automatically and pop up and editor to ask you 
  for a merge commit comment.
* Sometimes you need to do a manual merge. The chances increase if your team
  is all working on one file.

---
# Manual Merge
So it has all gone wrong. What happens now?

* The repository will enter a merging state.
* All files Git could figure out will be added to the Stage
* All files Git couldn't figure out will be decorated with "fishbones". Ex:
  `<<<<<<<`
* Make the changes you want to the file.
* Once your all done, you can do `git commit`. It will prompt you with 
  an automatic comment.
* Use `git checkout file.txt --theirs` or `--ours` to grab a copy of the 
  unmerged file either way.
* Use `git merge --abort` to give up and undo everything.

---
# Manual Merge Fishbones Example


    Here are lines that are either unchanged from the common
    ancestor, or cleanly resolved because only one side changed.
    <<<<<<< yours:sample.txt
    Conflict resolution is hard;
    let's go shopping.
    =======
    Git makes conflict resolution easy.
    >>>>>>> theirs:sample.txt
    And here is another line that is cleanly resolved or unmodified.


You could change it to this:

    Here are lines that are either unchanged from the common
    ancestor, or cleanly resolved because only one side changed.
    Conflict resolution is hard;
    let's go shopping.
    And here is another line that is cleanly resolved or unmodified.


And add the file to your stage


---
# Git Branches

* Only one branch at a time is visible. 
* To change a your branch you can use `git checkout CoolBranchName`.
* HEAD is the stuff that is in your current directory.
* Most of the time HEAD is pointing to a branch. 
    * When you commit a branch HEAD will automatically move forward to the new
      branch commit (version).
* Some times HEAD is pointing to a commit (version) or a tag. This is when
  your head is "detached". 
    * To "reattach" your HEAD simply checkout a branch.
* All repositories start off with a default branch of "master". Your
  repository probably has a master branch.
* You can merge a branch named `CoolFeature` into your currently 
  checked out branch by running `git merge CoolFeature`.


---
# More Git Information

*  https://git-scm.com/ - [Git Website](https://git-scm.com/)
*  https://try.github.io/ - [GitHub Git training](https://try.github.io/)
*  [Git Cheat Sheet](https://github.github.com/training-kit/downloads/github-git-cheat-sheet.pdf)
*  [Git Immersion](http://gitimmersion.com) - Interactive walk though.

![Git Logo](static/git-logo.png)

